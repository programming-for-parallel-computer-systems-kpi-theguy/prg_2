Second program for Course work of Programming for Parallel Computer Systems.

Purpose for the work: To create working example of program for Parallel Computer System with distributed memory, that can calculate math function using threads and specific language with it's mechansim.
Programming language: C++\MPI.
Computer System structure: Linear, with P number of the cores.
Used technologies: MPI library gives opportunity to create a working programs for parallel computer systems with distributed memory, that can easily communicate with nodes, using functions for sending or receiving messages between near nodes. .

Controling of the program:
Variable N stands for the size of Matrixes and Vectors respectively.
Variable P stands for the number of the threads, that program creates on the start, and works with them.
First thread will output the result.