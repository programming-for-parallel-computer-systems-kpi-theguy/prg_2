//------------------------------PfPCS---------------------------------------
//---------------------------Course Work------------------------------------
//-------PRG2.MPI. Sending and Receiving messages---------------------------
//--------------------------------------------------------------------------
//----------Task: e = max(t * X + B * (MA * MB))----------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 23.04.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include <mpi.h>
#include "data.h"
#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <limits.h>

using namespace std;

int N = 16;
int P = 16;

void ThreadFunction(int rank)
{
    MPI_Status status;

    int t;
    int e;
    int er = INT_MIN;
    Vector Z(N), X(N), B(N);
    Matrix MA(N, N), MB(N, N);

    if (P == 1)
    {
        //Input values of X and MA
        input_Vector(X);
        input_Matrix(MA);
        //Input values of B, t, MB
        input_Vector(B);
        input_Integer(t);
        input_Matrix(MB);
        //Calculating values of function
        sum_Vectors(multiply_Vector_Integer(X, t, rank), multiply_Vector_Matrix(multiply_Matrixes(MB, MA, rank), B, rank), Z, rank);

        //Calculating local maximum
        e = max_Vector(Z, rank);
        //Output result
        output_Integer(e);
    }
    else
    {
        if (rank < P / 2 - 1)
        {
            //If rank = 0, input values of X and MA
            if (rank == 0)
            {
                input_Vector(X);
                input_Matrix(MA);
            }
            //Else, receive Xh and MA from left task
            else
            {
                MPI_Recv((void*)((int*)&X+H*rank), N-rank*H, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
                MPI_Recv(&MA, N*N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
            }

            //Send Xh and MA to the right task
            MPI_Send((void*)((int*)&X+H*rank), N-(rank+1)*H, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
            MPI_Send(&MA, N*N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);

            //Receive B, t, MBh from the right task
            MPI_Recv(&B, N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(&t, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(&MB, (N-(P-1-rank)*H)*N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);

            //If rank > 0, send B, t, MBh to the left task
            if (rank > 0)
            {
                MPI_Send(&B, N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
                MPI_Send(&t, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
                MPI_Send(&MB, (N-(P-rank)*H)*N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
            }
        }
        else if (rank >= P / 2)
        {
            //If rank = P-1, input values of B, t, MB
            if (rank == P - 1)
            {
                input_Vector(B);
                input_Integer(t);
                input_Matrix(MB);
            }
            //Else, receive B, t, MBh from the right task
            else if(rank < P - 1)
            {
                MPI_Recv(&B, N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
                MPI_Recv(&t, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
                MPI_Recv(&MB, (N-(P-1-rank)*H)*N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
            }

            //Send B, t, MBh to the left task
            MPI_Send(&B, N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
            MPI_Send(&t, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
            MPI_Send(&MB, (N-(P-rank)*H)*N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);

            //Receive Xh and MA from the left task
            MPI_Recv((void*)((int*)&X+H*rank), N-rank*H, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(&MA, N*N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);

            //If rank < P - 1, send Xh and MA to the right task
            if (rank < P - 1)
            {
                MPI_Send((void*)((int*)&X+H*rank), N-(rank+1)*H, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
                MPI_Send(&MA, N*N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
            }
        }
        else if (rank == P / 2 - 1)
		{
			//Receive B, t, MBh from the right task
			MPI_Recv(&B, N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
			MPI_Recv(&t, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
			MPI_Recv(&MB, (N - (P - 1 - rank)*H)*N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);

            //If rank = 0, input values of X and MA
            if (rank == 0)
            {
                input_Vector(X);
                input_Matrix(MA);
            }
            //Else, receive Xh and MA from the left task
            else
            {
                MPI_Recv((void*)((int*)&X + H * rank), N - rank * H, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
                MPI_Recv(&MA, N*N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
            }
            //If rank > 0, send B, t, MBh to the left task
            if (rank > 0)
            {
                MPI_Send(&B, N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
                MPI_Send(&t, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
                MPI_Send(&MB, (N - (P - rank)*H)*N, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
            }

			//Send Xh and MA to the right task
			MPI_Send((void*)((int*)&X + H * rank), N - (rank + 1)*H, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
			MPI_Send(&MA, N*N, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
		}
    }

    //Calculating values of function
    sum_Vectors(multiply_Vector_Integer(X, t, rank), multiply_Vector_Matrix(multiply_Matrixes(MB, MA, rank), B, rank), Z, rank);

    //Calculating local maximum
    e = max_Vector(Z, rank);

    //If rank < P - 1, receive e(rank + 1) from the right task
    if(rank < P - 1)
    {
        MPI_Recv(&er, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);
    }

    //Calculating of global maximum
    if (er > e)
        e = er;

    //If rank > 0, send e to the left task
    if (rank > 0)
    {
        MPI_Send(&e, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
    }
    //If rank = 0, output result
    if (rank == 0)
    {
        output_Integer(e);
    }

}

int main(int argc, char *argv[])
{
	int rank;

	MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &P);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    cin.get();
    if(rank == 0)
    {
        cout << "Function started" << endl;
    }

    auto start = chrono::system_clock::now();
	
    ThreadFunction(rank);

	auto end = chrono::system_clock::now();

	chrono::duration<double> elapsed = end - start;
	if (rank == 0)
	{
		cout << "Elapsed time:" << elapsed.count() << endl;
	}

	MPI_Finalize();
	//cin.get();
    return 0;
}
