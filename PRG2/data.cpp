//------------------------------PfPCS---------------------------------------
//---------------------------Course Work------------------------------------
//-------PRG2.MPI. Sending and Receiving messages---------------------------
//--------------------------------------------------------------------------
//----------Task: e = max(t * X + B * (MA * MB))----------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 23.04.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include "data.h"
#include <iostream>


void input_Integer(int &a)
{
	a = 1;
}

void input_Vector(Vector &A)
{
	for (int i = 0; i < N; i++)
	{
		A[i] = 1;
	}
}

void input_Matrix(Matrix &MA)
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			MA[i][j] = 1;
		}
	}
}

void output_Integer(const int a)
{
	if (N < 10)
	{
		std::cout << std::endl;
		std::cout << a << std::endl;
		std::cout << std::endl;
	}
}

Matrix multiply_Matrixes(const Matrix &MA, const Matrix &MB, const int k)
{
	int cell;
	Matrix result(N, N);
	for (int i = H * k; i < H*(1 + k); i++)
	{
		for (int j = 0; j < N; j++)
		{
			cell = 0;
			for (int l = 0; l < N; l++)
			{
				cell += MA[i][l] * MB[l][j];
			}
			result[i][j] = cell;
		}
	}
	return result;
}

Vector multiply_Vector_Matrix(const Matrix &MA, const Vector &A, const int k)
{
	int cell;
	Vector result(N);
	for (int i = H * k; i < H*(1 + k); i++)
	{
		cell = 0;
		for (int j = 0; j < N; j++)
		{
			cell += A[j] * MA[i][j];
		}
		result[i] = cell;
	}
	return result;
}

Vector multiply_Vector_Integer(const Vector &A, const int a, const int k)
{
	Vector result(N);
	for (int i = H * k; i < H*(1 + k); i++)
	{
		result[i] = A[i] * a;
	}
	return result;
}

void sum_Vectors(const Vector &A, const Vector &B, Vector &C, const int k)
{
	for (int i = H * k; i < H*(1 + k); i++)
	{
		C[i] = A[i] + B[i];
	}
}

int max_Vector(const Vector &A, const int k)
{
	int result = A[H * k];
	for (int i = H * k; i < H*(1 + k); i++)
	{
		if (A[i] > result)
		{
			result = A[i];
		}
	}
	return result;
}